(function ($, window, document, undefined) {
  'use strict';

  $(function () {

    // SVG / PNG fallback
    if(!Modernizr.svg) {
      $('img[src*="svg"]').attr('src', function () {
        return $(this).attr('src').replace('.svg', '.png');
      });
    }

    $('.js-HomeSlider').slick();


  });

})(jQuery, window, document);